<!DOCTYPE HTML>
<html>

	<head>
		<title>{{title}}</title>
		<link rel="stylesheet" href="css/portfolio.css">
		<link href='http://fonts.googleapis.com/css?family=Alef:700' rel='stylesheet' type='text/css'>
		<script src="src/modernizr.js"></script>
	</head>

	<body>

		<div id="backgroundCanvas">
			<img src="img/bg.png"/>
		</div>

		<div id="container">

			<div id="header">
				<p>{{title}}</p>
			</div>

			<div id="nav">
				<ul id="navMenu">
					<li><a href="index.html">Home</a></li>
					<li><a href="showcase" {{^petProjects}}class="current"{{/petProjects}}>Showcase</a></li>
					<li><a href="pet-projects" {{#petProjects}}class="current"{{/petProjects}}>Pet Projects</a></li>
					<li><a href="contact">Contact</a></li>
				</ul>
			</div>

			<div id="content">

			{{#description}}
			<p>{{description}}</p>
			<hr/>
			{{/description}}

			{{#projects}}

				<img class="project-left" src="{{img}}"/>

				<div class="project-right">

					<h2>{{name}}</h2>
					<p>{{{description}}}</p>

					{{#url}}<p>Accessible <a href="{{url}}">here</a>{{/url}}{{#source}}  Code on Bitbucket <a href="{{source}}">here</a>{{/source}}</p>

					{{#tech}}
						<span>{{.}}</span>
					{{/tech}}

				</div>

				<br style="clear:left"/>

			{{/projects}}
				
			</div>

			<div id="footer">
			(C) Myra Jia Guerrier 2013
			</div>

			<div id="footer-img">&nbsp;</div>

		</div><!-- id="container" -->

		<script src="src/Loader.js"></script>
	</body>

</html>