<!DOCTYPE HTML>
<html>

	<head>
		<title>{{title}}</title>
		<link rel="stylesheet" href="css/portfolio.css">
		<link href='http://fonts.googleapis.com/css?family=Alef:700' rel='stylesheet' type='text/css'>
		<script src="src/modernizr.js"></script>
	</head>

	<body>

		<div id="backgroundCanvas">
			<img src="img/bg.png"/>
		</div>

		<div id="container">

			<div id="header">
				<p>{{title}}</p>
			</div>

			<div id="nav">
				<ul id="navMenu">
					<li><a href="index.html" {{#home}}class="current"{{/home}}>Home</a></li>
					<li><a href="projects">Projects</a></li>
					<li><a href="contact" {{#contact}}class="current"{{/contact}}>Contact</a></li>
				</ul>
			</div>

			<div id="content">

				{{{content}}}

			</div>

			<div id="footer">
			(C) Myra Jia Guerrier 2013
			</div>

			<div id="footer-img">&nbsp;</div>

		</div><!-- id="container" -->

		<script src="src/Loader.js"></script>
	</body>

</html>