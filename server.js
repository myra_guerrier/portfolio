var express = require("express"),
	mu = require("mustache"),
	fs = require("fs"),
	app = express.createServer();

// setup mustache templates for base site parameters

var genericTemplate = mu.compile( fs.readFileSync( "templates/index.mu", { encoding : "ascii" } ) ),
	projectTemplate = mu.compile( fs.readFileSync( "templates/project.mu", { encoding : "ascii" } ) ),
	projects = JSON.parse( fs.readFileSync( "templates/projects.json", { encoding : "ascii" } ) ),
	petProjects = JSON.parse( fs.readFileSync( "templates/petProjects.json", { encoding : "ascii" } ) ),
	index = fs.readFileSync( "client/index.html", { encoding : "ascii" } );

app.use(express.static(__dirname + "/client"));
console.log("Launched static file server successfully!");

app.get("/", function( req, res )
{
	res.send( index );
})

app.get("/showcase", function( req, res )
{
	var body = projectTemplate( projects );

	res.send( body );
});

app.get("/pet-projects", function( req, res )
{
	var body = projectTemplate( petProjects );

	res.send( body );
});

app.get("/contact", function( req, res )
{
	var body = genericTemplate( { title : "Contact me", contact : true, content :
		"<p>I can be reached at <a href=\"mailto:myra.jia.guerrier (@) gmail.com\">myra.jia.guerrier (@) gmail.com</a></p>" } );

	res.send( body );
})

app.listen(process.env.PORT || 3000);