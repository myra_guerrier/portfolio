// functions to add a Canvas to the app, and draw a background onto it

var app = app || {};

app.background =
(function()
{
	var firstRun = true,
		bgCanvas,
		canvas,
		ctx;
	
	// initializes the module
	this.init = function()
	{
		if(firstRun)
		{
			firstRun = false;
			
			// create the canvas object
			bgCanvas = $("#backgroundCanvas");
			canvas = $("<canvas />")[0];
			
			canvas.width = bgCanvas.width();
			canvas.height = bgCanvas.height();
			
			$(canvas).appendTo( bgCanvas );
			
			ctx = canvas.getContext("2d");
			app.graphics.setContext(ctx);
			
			// draw the background
			drawBG();
		}
	}
	
	// creates the background
	this.drawBG = function()
	{
		// basic parameters
		var x = 20,
			y = 20,
			stepIncrement = 10,
			scale = 0.5,
			range = 900,
			offsetAngle = 0.2*Math.PI;
			iter = 0;
	
		// create a gradient for filling
		var gradient = ctx.createLinearGradient(0, 0, 0, 480);
		gradient.addColorStop(0, "rgba(83, 95, 95, 0)");
		gradient.addColorStop(0.06060606060606061, "rgba(83, 95, 95, 0.07)");
		gradient.addColorStop(0.4494949494949495, "rgba(47, 127, 127, 0.54)");
		gradient.addColorStop(0.4898989898989899, "rgba(45, 131, 131, 0.59)");
		gradient.addColorStop(0.6818181818181818, "rgba(37, 153, 153, 0.36)");
		gradient.addColorStop(0.9797979797979798, "rgba(87, 105, 105, 0.03)");
		gradient.addColorStop(1, "rgba(87, 105, 105, 0)");
		ctx.strokeStyle = gradient;
	
		// we are creating a series of semi-random arcs out from a central point
		// first, define a function for generating a random arc segment
		var makeArc = function(r)
		{
			var start = (-Math.random()*Math.PI*scale) + offsetAngle,
				end = (Math.random()*Math.PI*scale) + offsetAngle;
				
			ctx.beginPath();
			ctx.arc(x, y, r, start, end);
			ctx.stroke();
		}
		
		// iterate through the range of values we've defined, creating random semi-circular arcs
		
		for(iter; iter < range; iter+=stepIncrement)
		{
			makeArc(iter);
		}
	}
	
	// give public access to those parameters which we want public
	return{
		init: init
	};
	
})();