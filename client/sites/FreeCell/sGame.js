var app = app || { };

// Create a generator for the properties of the Game screen
app.screen["gameScreen"] =
(
	function()
	{
		var firstRun = true;
	
		// Called whenever the screen is enabled
		function run()
		{
			if(firstRun)
			{
				firstRun = false;
				init();
			}
			
			// start a new FreeCell game
			app.freeCellModel.startGame();
			app.freeCellView.setup(app.freeCellModel);
			app.freeCellView.update();
			setTimeout(app.freeCellView.deal, 250);
			app.freeCellController.init();
			app.freeCellAudio.init();
			//app.freeCellAudio.playSound("music");
			
			// increment the games played counter, since we just started a new one
			app.stats.played++;
			app.storage.set("stats", app.stats);
			
			// update the stats display
			$("#statsLabel").text("Games won "+app.stats.won+" / "+app.stats.played);
		}
		
		// Called once, when the screen is first opened.
		function init()
		{
			// set up a handler for the exit button
			$("#gameScreen #exitButton").click(
				function( event )
				{
					app.freeCellView.shutdown();
					app.showScreen("menuScreen");
					return false;
				}
			);
		}
		
		// Returns whatever parameters we want accessible publically
		return {
			run: run
		};
	}
)();