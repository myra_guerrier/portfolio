var app = app || { };

// Definitions for the Screen object
app.screen = {};

// defintions for a global application settings object
app.settings = {};

// definitions for a win/loss statistics object
app.stats = {
	won:0, played:0
}

// Screen switch function, used for switching between screens
app.showScreen = function( screenID )
{
	var activeScreen, screen,
		args = Array.prototype.slice.call(arguments, 1),
		newScreen;
	
	// stop all playing sounds
	app.audio.stopAll();
	
	activeScreen = $("#game .screen.active");
	screen = $("#" + screenID);
	newScreen = app.screen[screenID];
	
	if (activeScreen)
	{
		activeScreen.removeClass("active");
	}
	
	console.log("Debug: switching to screen: "+screenID);
	
	screen.addClass("active");
	newScreen.run.apply(newScreen, args);
}

// Startup function, called when the page finishes loading
app.start = function()
{
	// set up client-side storage
	app.storage.setPrefix("freeCell_");
	
	// load win/loss stats
	app.stats = app.storage.get("stats") || app.stats;

	// create canvas
	app.background.init();
	app.showScreen("splashScreen", app.loader.getLoadProgress);
}