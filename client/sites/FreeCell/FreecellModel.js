// Contains a model of the FreeCell game
// Basic API operations:
// * pickUp() and putDown() take the top card from location loc, position i and place at location loc, position i.  They perform checking automatically, and only move if it is valid to do so.
// * tableaux = t, cell = c, foundation = f
// * checkVictory() determines whether the game has been won, and should be called after every successful move by the controller.

var app = app || {};

app.freeCellModel = 
(function()
{

	var deck,
		tableaux = [[],[],[],[],[],[],[],[]],
		cells = [],
		foundations = [[],[],[],[]],
		hand = [],
		// we diverge a bit from a strict model-view-controller paradigm for simplicity, animated
		// cards are placed here
		motionArray = {},
		cardInHand = false;
	
	// clear the game state
	function resetGame()
	{
		deck = null;
		tableaux = [[],[],[],[],[],[],[],[]];
		cells = [];
		foundations = [[],[],[],[]];
		hand = [];
		motionArray = {};
		cardInHand = false;
	}
	
	// create a new game and assign cards from the deck to the tableaux
	function startGame()
	{
		// Note: the game startup animation sets up all of this, so it is commented out for now
	
		/*var d = app.playingCards.newDeck(true),
			iter = 0;
		
		// iterate through the tableaux, popping elements off of d and assigning them to the specified cell
		while(d.length > 0)
		{
			tableaux[iter++].push(d.pop());
			if(iter > 7)
				iter = 0;
		}*/
	}
	
	// returns the top card in the specified portion of the tableaux
	function getTableauxTopcard(index)
	{
		if(tableaux[index].length > 0)
			return tableaux[index][tableaux[index].length - 1];
		else
			return null;
	}
	
	// returns the content of the current cell
	function getCellContents(index)
	{
		return cells[index];
	}
	
	// moves the card at index i from location loc to new location newLoc
	// Note: does not test for whether this is a valid move, just performs the move.
	function moveCard(loc, i, newLoc, j)
	{
		newLoc[j] = loc.slice(i,i+1)[0];
	}
	
	// moves a card from the specified location to the animation array
	// NOTE: if loc is set to "none", don't pull the card from anywhere, and treat i as a card
	function moveToMotionArray(loc, i, ref)
	{
		switch(loc)
		{
			case "t":
				motionArray[ref] = tableaux[i].pop();
				break;
			case "c":
				motionArray[ref] = cells[i];
				cells[i] = null;
				break;
			case "f":
				motionArray[ref] = foundations[i].pop();
				break;
			case "h":
				console.log(hand[0]);
				motionArray[ref] = hand.pop();
				cardInHand = false;
				break;
			case "none":
				motionArray[ref] = i;
				break;
		}
	}
	
	// takes a card from the animation array and moves it to the specified field location
	// NOTE: does not check for the validity of moves
	function moveFromMotionArray(loc, i, ref)
	{
		switch(loc)
		{
			case "t":
				tableaux[i].push(motionArray[ref]);
				motionArray[ref] = null;
				break;
			case "c":
				cells[i] = motionArray[ref];
				motionArray[ref] = null;
				break;
			case "f":
				foundations[i].push(motionArray[ref]);
				motionArray[ref] = null;
				break;
		}
	}
	
	// picks up a card (moves it to the Hand)
	function pickUp(loc, i)
	{
		console.log("Attempted pick-up!");
	
		if(cardInHand === true)
		{
			console.log("Tried to pick up a card with one already in hand!");
			return false;
		}
	
		switch(loc)
		{
			case "t":
				hand[0] = tableaux[i].pop();
				hand[0].lastPos = {loc:"t",i:i};
				cardInHand = true;
				return true;
				break;
			case "c":
				hand[0] = cells[i];
				hand[0].lastPos = {loc:"c",i:i};
				cells[i] = null;
				cardInHand = true;
				return true;
				break;
		}
		
		return "Error!  Invalid coordinates specified";
	}
	
	// attempts to put down a card, if this is impossible, log an error and return false
	function putDown(loc, i)
	{
		console.log("Attempted put-down!");
	
		if(cardInHand === false)
		{
			console.log("No card in hand, cannot put down card!");
			return false;
		}
	
		switch(loc)
		{
			case "t":
				if(!getTableauxTopcard(i) || checkSequential(getTableauxTopcard(i), hand[0]))
				{
					console.log("Moving "+hand[0].getShortName()+" to Tableaux "+i+"...");
					tableaux[i].push(hand.pop());
					cardInHand = false;
					return true;
				}
				else
				{
					console.log("Invalid move: attempted to place "+hand[0].getShortName()+" on Tableaux "+i+": "+getTableauxTopcard(i).getShortName());
					return false;
				}
				break;
			
			case "c":
				// cells[i] is already occupied
				if(cells[i])
				{
					console.log("Invalid move: attempted to place "+hand[0].getShortName()+" on Cell "+i+" containing "+cells[i].getShortName());
					return false;
				}
				else
				{
					cells[i] = hand.pop();
					cardInHand = false;
					return true;
				}
				break;
				
			case "f":
				// if foundation[i] is null, it's empty, so just move right away
				if(foundations[i][0] !== true && hand[0].num === 1)
				{
					console.log("Moving "+hand[0].getShortName()+" to Foundation "+i+"!");
					foundations[i].push(hand.pop());
					cardInHand = false;
					return true;
				}
				
				// if not, check in more detail
				else if(foundations[i][foundations[i].length-1].suite === hand[0].suite)
				{
					// check for ascending number
					if(foundations[i][foundations[i].length-1].num + 1 === hand[0].num)
					{
						console.log("Moving "+hand[0].getShortName()+" to Foundation "+i+"!");
						foundations[i].push(hand.pop());
						cardInHand = false;
						return true;
					}
				}
				
				console.log("Invalid move: attempted to place "+hand[0].getShortName()+" on foundation "+i+" containing "+foundations[i][foundations[i].length-1].getShortName());
				cardInHand = true;
				return false;
				break;
		}
	}
	
	// checks if c2 can be placed after c1 in the Tableaux
	function checkSequential(c1, c2)
	{
		// check for color, don't allow placement of the same-colored cards next to each other
		if(c1.color !== c2.color)
		{
			// check for order
			if(c1.num - 1 === c2.num)
				return true;
		}
		
		return false;
	}
	
	// checks if the game has been won
	function checkVictory()
	{
		var i;
	
		for(i; i<4; i++)
		{
			if(foundations[i][foundations[i].length-1].num !== 13)
				return false;
		}
		
		return true;
	}
	
	// fakes a victory condition, by moving the cards into a specific configuration
	function fakeVictory()
	{
		var d = app.playingCards.newDeck(),
			i = 0;
	
		tableaux = [[],[],[],[],[],[],[],[]];
		cells = [];
		foundations = [[],[],[],[]];
		hand = [];
		
		while(d.length > 0)
		{
			foundations[i++].push(d.pop());
			if(i > 3)
				i = 0;
		}
		
		app.freeCellController.winGame();
	}
	
	// prints the state of the game
	function print()
	{
		var output = [],
			i=0,j=0,k=0,l=0;
		
		output.push("Cells: ");
		for(i; i<4; i++)
		{
			if(getCellContents(i))
				output.push(i + ": " + getCellContents(i).getShortName());
		}
		
		output.push("\n");
		output.push("Foundations: ");
		
		for(l; l<4; l++)
		{
			if(foundations[l])
				output.push(l+": " + foundations[l][foundations[l].length-1].getShortName());
		}
		
		output.push("\n");
		output.push("Tableaux: \n");
		
		for(j; j<8; j++)
		{
			output.push(j + ": ")
			k = 0;
			for(k; k<tableaux[j].length; k++)
				output.push(tableaux[j][k].getShortName());
			output.push("\n");
		}
		
		output.push("Hand: ");
		if(hand[0])
			output.push(hand[0].getShortName());
		
		console.log(output.join());
	}
	
	// PUBLIC ACCESSOR FUNCTIONS:
	function getDeck()
	{
		return deck;
	}
	
	function getTableaux(iter)
	{
		if(typeof(iter) === "undefined")
			return tableaux;
		else
			return tableaux[iter];
	}
	
	function getCells()
	{
		return cells;
	}
	
	function getFoundations(full)
	{
		if(full === true)
			return foundations;
	
		return [
			foundations[0][foundations[0].length-1],
			foundations[1][foundations[1].length-1],
			foundations[2][foundations[2].length-1],
			foundations[3][foundations[3].length-1]
			];
	}
	
	function getHand()
	{
		return hand;
	}
	
	function getCardInHand()
	{
		return cardInHand;
	}
	
	// return
	return{
		startGame: startGame,
		resetGame: resetGame,
		pickUp: pickUp,
		putDown: putDown,
		moveCard: moveCard,
		checkVictory: checkVictory,
		fakeVictory: fakeVictory,
		print: print,
		
		getDeck: getDeck,
		getTableaux: getTableaux,
		getCells: getCells,
		getFoundations: getFoundations,
		getHand: getHand,
		
		getCardInHand: getCardInHand,
		
		moveToMotionArray: moveToMotionArray,
		moveFromMotionArray: moveFromMotionArray
	}
		
})();