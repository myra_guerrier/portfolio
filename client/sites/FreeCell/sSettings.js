var app = app || { };

// Create a generator for the properties of the Settings screen
app.screen["settingsScreen"] =
(
	function()
	{
		var firstRun = true,
			decks = ["tarot","classic","light"],
			startingDeck = 0;
	
		// Called whenever the screen is enabled
		function run()
		{
			if(firstRun)
			{
				firstRun = false;
				init();
			}
		}
		
		// update the application's settings
		// if no arguments are given, loads the settings from clientStorage
		// if an argument is given, pushes the settings to the client
		function updateSettings(sObj)
		{
			if(sObj)
			{
				app.storage.set("settings",sObj);
			}
			else
			{
				app.settings = app.storage.get("settings") || app.settings;
			}
			
			// update the state of the settings screen, in either case
			
			// sound
			if(app.settings.soundOn)
				$("#toggleSound").text("On");
			else
				$("#toggleSound").text("Off");
				
			// deck selector
			switch(app.settings.deck)
			{
				// Tarot
				case 0:
					$("#setDeck").text("Tarot");
					
					$("#tarot").show();
					$("#classic").hide();
					$("#light").hide();
					break;
				
				// classic
				case 1:
					$("#setDeck").text("Classic");
				
					$("#tarot").hide();
					$("#classic").show();
					$("#light").hide();
					break;
					
				// light
				case 2:
					$("#setDeck").text("Light");
				
					$("#tarot").hide();
					$("#classic").hide();
					$("#light").show();
					break;
					
				default:
					console.log("Error!  Deck selector index out of range!");
					break;
			}
			
			// show reload message
			if(startingDeck === app.settings.deck)
				$("#reloadReminder").text("");
			else
				$("#reloadReminder").text("To see the new deck, reload the web page!");
		}
		
		// Called once, when the screen is first opened.
		function init()
		{
			// set up default settings
			app.settings.soundOn = true;
			app.settings.deck = 0;
			
			// try getting settings, to see if any have been saved before
			// if any have been set before, this will override the defaults
			updateSettings();
			
			// set the default deck so we can tell the user when they should reload the page
			startingDeck = app.settings.deck || 0;
			$("#reloadReminder").text("");
		
			// set up a handler for the audio state
			$("#settingsScreen #toggleSound").click(
				function(event)
				{
					app.settings.soundOn = !app.settings.soundOn;
					updateSettings(app.settings);
					return false;
				}
			);
		
			// handler for the deck selector function
			$("#settingsScreen #setDeck").click(
				function()
				{
					// iterate through the possible decks
					app.settings.deck = app.settings.deck + 1;
					if(app.settings.deck > decks.length - 1)
						app.settings.deck = 0;
						
					updateSettings(app.settings);
					return false;
				}
			);
		
			// set up a handler for the ok button
			$("#settingsScreen #okButton").click(
				function( event )
				{
					app.showScreen("menuScreen");
					return false;
				}
			);
		}
		
		// Returns whatever parameters we want accessible publically
		return {
			run: run
		};
	}
)();