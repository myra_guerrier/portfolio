var app = app || { };

app.loader = 
(function()
{
	try
	{
		var deck = JSON.parse(localStorage["freeCell_settings"]).deck || 0;
	}
	catch(err)
	{
		deck = 0;
	}
	
	var resourceList = generateResourceList(),
		numResourcesLoaded = 0,
		numResourcesToLoad = 0;
	
	// we support three decks, each named in a slightly different way
	// there are three versions of the getCardURL function, set based on the deck
	// case 0: tarot deck:
	function getTarotDeckURL( card )
	{
		var url = "cards/tarot/",
			num = 0;
			
		// account for the leading 0s in the card filenames
		if(card.num.toString().length < 2)
			url += "00" + card.num;
		else
			url += "0" + card.num;
		
		switch(card.suite)
		{
			case "c":
				url += "c.gif";
				break;
			case "d":
				url += "d.gif";
				break;
			case "h":
				url += "h.gif";
				break;
			case "s":
				url += "s.gif";
				break;
			default:
				console.log("Something went wrong when fetching card URL: ");
				console.log(card);
		}
		
		return url;
	}
	
	// case 1: classic deck
	function getClassicDeckURL( card )
	{
		var url = "cards/classic/";
		
		// add the suite
		url += card.suite;
		
		// if we're under 11, use the number as is, if not, append a letter to handle jacks, queens, and kings
		if(card.num <= 10)
		{
			url += card.num;
		}
		else
		{
			switch(card.num)
			{
				case 11:
					url += "j";
					break;
				case 12:
					url += "q";
					break;
				case 13:
					url += "k";
					break;
				default:
					console.log("Something went wrong when loading "+card+"!");
					break;
			}
		}
		
		// add the file extension
		url += ".gif";
		
		return url;
	}
	
	// case 2: light deck
	function getLightDeckURL( card )
	{
		var url = "cards/light/",
			multiplier = 0;
			
		// set the multiplier
		switch(card.suite)
		{
			case "d":
				multiplier = 0;
				break;
			case "c":
				multiplier = 1;
				break;
			case "h":
				multiplier = 2;
				break;
			case "s":
				multiplier = 3;
				break;
		}
		
		url += (100 + (multiplier * 13) + card.num);
		
		url += ".gif";
		
		return url;
	}
	
	// returns a text string containing the url of a card's image file
	function getCardURL( card )
	{
		// use one of the above functions based on which deck is selected
		switch( deck )
		{
			case 0:
				return getTarotDeckURL( card );
				break;
			case 1:
				return getClassicDeckURL( card );
				break;
			case 2:
				return getLightDeckURL( card );
				break;
			default:
				return getTarotDeckURL( card );
				break;
		}
	}
	
	// returns how much of the site data has been loaded
	function getLoadProgress()
	{
		if(numResourcesToLoad > 0)
			return numResourcesLoaded / numResourcesToLoad;
		else
			return 1;
	}
	
	// populate the resource list, with cards
	function generateResourceList()
	{
		var cardList = [],
		generateCard = function(n,s) { var c = {}; c.num = n; c.suite = s; return c; },
		i = 1, s = 0,
		suiteList = ["c","d","h","s"];
		
		for(s; s<4; s++)
		{
			for(i; i<=13; i++)
			{
				cardList.push("resource!" + getCardURL(generateCard(i,suiteList[s])));
			}
			
			i = 1;
		}
		
		return cardList;
	}
	
	// Runs the loader, getting all resources
	function run()
	{
		numResourcesToLoad = resourceList.length;
		numResourcesLoaded = 0;
	
		// set up the resource! prefix so that we don't try to execute SVG files as a javascript
		yepnope.addPrefix("resource",
			function( resourceObj )
			{
				resourceObj.noexec = true;
				return resourceObj;
			});
	
		// Load the scripts with Modernizr
		Modernizr.load(
			{
				load : ["jquery.js","App.js","sSplash.js", "sMenu.js", "sGame.js", "sSettings.js", "sAbout.js","CanvasGraphics.js","BackgroundCanvas.js", "PlayingCards.js",
				"FreecellModel.js", "FreecellView.js", "FreecellController.js", "FreecellAudio.js",
				"Vector.js", "Time.js", "Audio.js", "Storage.js"],
				complete : function() { app.start(); }
			}
		);
		
		// Modernizr load call fails, using yepnope directly
		yepnope(
			{
				load : resourceList,
				callback : function(url, result, key)
				{
					numResourcesLoaded++;
				},
				complete : function()
				{
					numResourcesLoaded = numResourcesToLoad;
				}
			}
		);
	}
	
	// Returns what should be publically accessible
	return {
		run: run,
		getCardURL: getCardURL,
		getLoadProgress: getLoadProgress
	}
	
})();


app.loader.run()