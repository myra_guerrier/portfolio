﻿// Library for playing cards and decks

var app = app || {};

app.playingCards = 
(function()
{

	// Generate a card object
	function card(num, suite, id, lastPos)
	{
		this.num = num;
		this.suite = suite;
		this.id = id;
		
		if(jQuery.inArray(suite, ["c","s"]) > -1)
			this.color = "b";
		else
			this.color = "r";
			
		if(typeof(lastPos) !== "undefined")
			this.lastPos = lastPos;
		else
			this.lastPos = {loc:"none", i:0};
		
		
		// returns a string containing the full name of the card
		this.getName = function()
		{
			var n;
			
			// special cases
			switch(num)
			{
				case 1:
					n = "Ace";
					break;
				case 11:
					n = "Jack";
					break;
				case 12:
					n = "Queen";
					break;
				case 13:
					n = "King";
					break;
				default:
					n = num;
			}
			
			n += " of ";
			
			// suite
			switch(suite)
			{
				case "s":
					n += "Spades";
					break;
				case "d":
					n += "Diamonds";
					break;
				case "h":
					n += "Hearts";
				case "c":
					n += "Clubs";
				default:
					n += "Suite Error";
			}
			
			return n;
		}
		
		this.getShortName = function()
		{
			var n;
			
			switch(num)
			{
				case 1:
					n = "A";
					break;
				case 11:
					n = "J";
					break;
				case 12:
					n = "Q";
					break;
				case 13:
					n = "K";
					break;
				default:
					n = num;
			}
			
			switch(suite)
			{
				case "s":
					n += "♠";
					break;
				case "d":
					n += "♦";
					break;
				case "h":
					n += "♥";
					break;
				case "c":
					n += "♣";
					break;
				default:
					n += "Suite Error";
			}
			
			return n;
		}
		
		return this;
	}
	
	// Generate a deck
	function newDeck(shuffled)
	{
		var deck = [],
			i,
			suites = ["s","d","h","c"],
			idIter = 0;
		
		for(suite in suites)
		{
			i = 1;
		
			for(i; i<=13; i++)
			{
				deck[deck.length] = new this.card(i, suites[suite], idIter++);
			}
		}
		
		
		if(typeof(shuffled) !== "undefined")
		{
			for (var j = deck.length - 1; j > 0; j--)
			{
				var k = Math.floor(Math.random() * (j + 1));
				var temp = deck[j];
				deck[j] = deck[k];
				deck[k] = temp;
			}
		}
		
		return deck;
	}

	return {
		card: card,
		newDeck: newDeck
	}
})();