var app = app || { };

// Create a generator for the properties of the Menu screen
app.screen["menuScreen"] =
(
	function()
	{
		var firstRun = true;
	
		// Called whenever the screen is enabled
		function run()
		{
			if(firstRun)
			{
				firstRun = false;
				init();
			}
		}
		
		// Called once, when the screen is first opened.
		function init()
		{
			// Create a handler to set the current screen based on which button is pressed
			$("#menuScreen menu").click(
				function( event )
				{
					var target = $(event.target),
						screenID;
						
					if (target.is("button"))
					{
						screenID = target.attr("name");
						app.showScreen(screenID);
					}
					
					return false;
				}
			);
		}
		
		// Returns whatever parameters we want accessible publically
		return {
			run: run
		};
	}
)();