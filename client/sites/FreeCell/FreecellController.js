// provide functions for controlling the freecell game

var app = app || {};

app.freeCellController = 
(function()
{
	var inputEnabled = true;
	
	// disables clicks
	function disableInput()
	{
		console.log("Disabling input!");
		inputEnabled = false;
	}
	
	// enables clicks
	function enableInput()
	{
		console.log("Enabling input!");
		inputEnabled = true;
	}

	// create handlers
	function init()
	{
		// handler for clicking on a card
		$("#gameScreen").click(
			function(event)
			{
				if(inputEnabled === false)
					return;
			
				// play a sound effect
				app.freeCellAudio.playSound("click");
			
				var x = event.clientX-$(this).offset().left,
					y = event.clientY-$(this).offset().top,
					click = app.freeCellView.getClickLocation(x,y),
					card;
			
				if(click === null)
				{
					console.log("Not a valid click location");
					return false;
				}
			
				if(app.freeCellModel.getCardInHand() === false)
				{
					if(app.freeCellModel.pickUp(click.pos, click.i) === true)
					{
					}
					else
					{
					}
				}
				else if(app.freeCellModel.getCardInHand() === true)
				{
					if(app.freeCellModel.putDown(click.pos, click.i) === true)
					{
					}
					else
					{
						card = app.freeCellModel.getHand()[0];
						
						app.freeCellView.registerCardAnimation(
							card=card,
							start={x:x, y:y},
							end=app.freeCellView.getCoordinates(
									card.lastPos.loc,
									card.lastPos.i),
							velocity=20,
							from={loc:"h", i:0},
							destination={loc:card.lastPos.loc, i:card.lastPos.i},
							true
						);
					}
				}
				
				return false;
			}
		);
		
		// handler for mouse movements in general
		$("#gameScreen").mousemove(
			function(event)
			{
				var x = event.clientX-$(this).offset().left,
					y = event.clientY-$(this).offset().top;
					
				app.freeCellView.updateMousePos(x, y);
			}
		);
	}
	
	// wins the game
	function winGame()
	{
		// increment the games played counter, since we just started a new one
		app.stats.won++;
		app.storage.set("stats", app.stats);
	
		app.freeCellView.win();
		setTimeout(app.freeCellView.shutdown,
			5000);
	}
	
	return{
		enableInput: enableInput,
		disableInput: disableInput,
		init:init,
		winGame: winGame
	}
})();