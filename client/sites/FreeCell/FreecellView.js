// uses a canvas to contain the view of the freecell model

var app = app || {};

app.freeCellView = 
(function()
{
	var freecellModel,
		freecellDiv,
		canvas,
		ctx,
		cardImages = {},
		sortedDeck,
		c = 0,
		boardParams = {},
		animObjectList = [],
		animTimeout = 0,
		cardsInMotion = 0,
		lastUpdateTime = 0,
		mouseX,
		mouseY,
		d = app.playingCards.newDeck(true),
		iter = 0;
		
	// ------------------------------------------------------------------------
	// ANIMATION HANDLING
	// ------------------------------------------------------------------------
	
	// registers a card to animate, moving between two points, and disables input while the card is in motion
	//
	// card - a Card Object
	// start - a vector, containing the start position of the card in card widths
	// end - a vector, containing the end position in card widths
	// velocity - speed of animation, in card-widths per second
	// from - (optional) the location to take the card from, if it is not originating offscreen
	// destination - (optional) the location within the game that the card should end up
	// inPixels - if set, interpret the input coordinates in pixels, not in view units
	function registerCardAnimation(card, start, end, velocity, from, destination, inPixels)
	{
		var animationObject = {};
		
		animationObject.card = card;
		// convert start and end into pixels
		if(inPixels !== true)
		{
			animationObject.start = app.vector.scalarMul(start, boardParams.cardWidth);
			animationObject.end = app.vector.scalarMul(end, boardParams.cardWidth);
		}
		else
		{
			animationObject.start = start;
			animationObject.end = end;
		}
		
		animationObject.x = animationObject.start.x;
		animationObject.y = animationObject.start.y;
		
		// convert velocity into a number in pixels per second
		animationObject.velocity = velocity * boardParams.cardWidth;
		
		// generate a direction vector based on the start and end coordinates
		animationObject.direction = app.vector.normalize(app.vector.subtract(
			animationObject.end,
			animationObject.start));
			
		// calculate transit time in seconds based on velocity
		animationObject.transitTime = app.vector.length(
			app.vector.subtract(
				animationObject.end,
				animationObject.start
			)
		) / (animationObject.velocity);
		
		// an unclear bug causes the above code to give a slightly incorrect answer, a functional
		// approximation of the correct one is found by multiplying the value by 1.22
		
		//animationObject.transitTime = animationObject.transitTime * 1.22;
		
		// move the card
		animationObject.from = from;
		freecellModel.moveToMotionArray(from.loc, from.i, animationObject.card.id);
		
		// check if we should be putting this card anywhere
		if(destination)
			animationObject.destination = destination;
			
		// create an update function for the card
		animationObject.update = function(deltaTime)
		{
			animationObject.x += animationObject.direction.x * animationObject.velocity * deltaTime;
			animationObject.y += animationObject.direction.y * animationObject.velocity * deltaTime;
		}
			
		// add the object to the animation object list
		animObjectList[animationObject.card.id] = animationObject;
		
		// start the animation
		startAnimation(animationObject.card.id, (animationObject.transitTime * 1000) );
	}
	
	// starts an animation, placing a timeout on input processing
	function startAnimation(id, timeout)
	{
		// sets the global animation timeout, if it's not already greater than this animation
		if(timeout > animTimeout)
		{
			animTimeout = timeout;
		}
			
		// increment the cards in motion counter -- if this is greater than zero, we disable input
		// it gets decremented when the card's motion finishes
		cardsInMotion++;
		app.freeCellController.disableInput();
			
		// after the animation timeout finishes, end the animation, and place the card, if relevent
		setTimeout(
			function()
			{
				if(animObjectList[id].destination)
				{
					freecellModel.moveFromMotionArray(
						animObjectList[id].destination.loc,
						animObjectList[id].destination.i,
						animObjectList[id].card.id);
				}
				
				endAnimation(id);
			},
			timeout);
	}
	
	// ends an animation
	function endAnimation(id)
	{
		cardsInMotion--;
		if(cardsInMotion <= 0)
			app.freeCellController.enableInput();
			
		animObjectList[id] = null;
	}
		
	// ------------------------------------------------------------------------
	// EFFECTS HANDLING
	// ------------------------------------------------------------------------
	
	// animates the act of dealing a new game
	function deal()
	{
		var card = d.pop();
		
		registerCardAnimation(
				card,
				{x:0,y:0},
				{
					x:0.2 + (iter * 1.1),
					y:(boardParams.cardHeight/boardParams.cardWidth) + 
						0.3 + 
						(freecellModel.getTableaux(iter).length * 0.3 * 
							(boardParams.cardHeight/boardParams.cardWidth))
				},
				15,
				{loc:"none", i:card},
				{loc:"t", i:iter++}
			);
			
		if(iter > 7)
			iter = 0;
			
		if(d.length > 0)
		{
			setTimeout(
				deal,
				100
			);
		}
	}
	
	// animates the game-end animation and displays an appropriate message
	function win()
	{
		var c = 0,
		cInner = 0,
		f = freecellModel.getFoundations(true),
		card;
	
		// iterate through the foundations, giving all the elements in them an animation
		for(c; c<4; c++)
		{
			cInner = 12;
		
			for(cInner; cInner>0; cInner--)
			{
				card = f[c][cInner];
			
				registerCardAnimation(
					card,
					getCoordinates("f",c),
					{
						x:getCoordinates("f",c).x + (Math.random()*201)-100,
						y:1000+(Math.random()*101)-50
					},
					3+(Math.random()*3)-1,
					{loc:"none", i:card},
					{loc:"none", i:card},
					inPixels=true
				);
			}
		}
	}
		
	// ------------------------------------------------------------------------
	// DISPLAY HANDLING
	// ------------------------------------------------------------------------
	
	// update the display of the game state
	function update(cancelUpdate)
	{
		var c = 0, f = 0, t = 0, tInner = 0,
			cells = freecellModel.getCells(),
			foundations = freecellModel.getFoundations(),
			tableaux = freecellModel.getTableaux(),
			hand = freecellModel.getHand()[0],
			deltaTime = app.time.getTime() - lastUpdateTime;
		
		// first, check if cancelUpdate has been set, if so, return immediately, 'breaking' the update cycle
		if(cancelUpdate === true)
			return;
		
		ctx.clearRect(0,0,freecellDiv.width(),freecellDiv.height());
		
		drawBackgroundElements()
		
		ctx.shadowBlur = 0;
		
		// draw cell contents
		for(c; c<4; c++)
		{
			if(cells[c])
				ctx.drawImage(cardImages[cells[c].id],
					boardParams.c1x + (boardParams.cardWidth * (1.1 * c)),
					boardParams.cellTop,
					boardParams.cardWidth,
					boardParams.cardHeight);
		}
		
		// draw foundation contents
		for(f; f<4; f++)
		{
			if(foundations[f])
				ctx.drawImage(cardImages[foundations[f].id],
					boardParams.f1x + (boardParams.cardWidth * (1.1 * f)),
					boardParams.cellTop,
					boardParams.cardWidth,
					boardParams.cardHeight);
		}
		
		// draw tableaux contents
		for(t; t<8; t++)
		{
			tInner = 0;
			
			if(typeof(tableaux[t]) !== "null")
			{
				for(tInner; tInner<tableaux[t].length; tInner++)
				{
					ctx.drawImage(cardImages[tableaux[t][tInner].id],
						boardParams.t1x + (boardParams.cardWidth * (1.1 * t)),
						boardParams.tableauxTop +
							((boardParams.maxToffset * tInner)),
						boardParams.cardWidth,
						boardParams.cardHeight);
				}
			}
		}
		
		// draw the picked-up card
		if(hand)
		{
			ctx.shadowBlur = 10;
			ctx.shadowColor = "rgba(0,0,0,1)";
			ctx.drawImage(cardImages[hand.id],
				mouseX,
				mouseY,
				boardParams.cardWidth,
				boardParams.cardHeight);
		}
		
		// draw all in-motion cards
		for(obj in animObjectList)
		{
			/*ctx.shadowBlur = 10;
			ctx.shadowColor = "rgba(0,0,0,1)";*/
			
			if(animObjectList.hasOwnProperty(obj))
			{
				if(animObjectList[obj] !== null)
				{
					// update the card's position
					
					animObjectList[obj].update(deltaTime);
					
					// draw the card at the updated position
					ctx.drawImage(cardImages[
							animObjectList[obj].card.id
						],
						animObjectList[obj].x,
						animObjectList[obj].y,
						boardParams.cardWidth,
						boardParams.cardHeight);
				}
			}
		}
		
		// set the last updated time
		lastUpdateTime = app.time.getTime();
		
		// after a short timeout, run update again
		// TODO: replace this with requestAnimationFrame()
		setTimeout(update, 30);
	}

	// draw some background frames
	function drawBackgroundElements()
	{
		var c=0, f=0, t=0;
	
		ctx.fillStyle = "rgba(255,255,255,0.5)";
		ctx.shadowColor = "rgba(0,0,0,0.4)";
		ctx.shadowBlur = 5;
	
		// cells
		for(c; c<4; c++)
		{
			app.graphics.filledRoundRect(
				boardParams.leftMargin + (boardParams.cardWidth * (1.1 * c)),
				boardParams.cellTop,
				boardParams.cardWidth,
				boardParams.cardHeight,
				5);
		}
		
		// foundations
		for(f; f<4; f++)
		{
			app.graphics.filledRoundRect(
				boardParams.f1x + (boardParams.cardWidth * (1.1 * f)),
				boardParams.cellTop,
				boardParams.cardWidth,
				boardParams.cardHeight,
				5);
		}
		
		// tableaux
		for(t; t<8; t++)
		{
			app.graphics.filledRoundRect(
				boardParams.t1x + (boardParams.cardWidth * (1.1 * t)),
				boardParams.tableauxTop,
				boardParams.cardWidth,
				boardParams.cardHeight,
				5);
		}
	}
	
	// Set up the game view
	function setup(model)
	{
		sortedDeck = app.playingCards.newDeck();
	
		freecellModel = model;
	
		freecellDiv = $("#gameScreenCanvas");
		
		canvas = $("<canvas />")[0];
		canvas.width = freecellDiv.width();
		canvas.height = freecellDiv.height();
		
		$(canvas).appendTo(freecellDiv);
		
		ctx = canvas.getContext("2d");
		app.graphics.setContext(ctx);
		
		// calculate the base card pixel dimensions
		var tempCard = $("<img />"),
			cardX, cardY;
		tempCard[0].src = app.loader.getCardURL(sortedDeck[0]);
		cardX = tempCard[0].width;
		cardY = tempCard[0].height;
		tempCard.remove();
		
		// set up the board parameters
		boardParams.cardWidth = freecellDiv.width() / 10.1;
		boardParams.cardHeight = boardParams.cardWidth * (cardY/cardX); // account for aspect ratio
		boardParams.leftMargin = 0.5 * boardParams.cardWidth;
		boardParams.cellTop = 0.2 * boardParams.cardWidth;
		boardParams.c1x = boardParams.leftMargin;
		boardParams.f1x = boardParams.leftMargin + (boardParams.cardWidth * 4.8);
		boardParams.tableauxTop = boardParams.cardHeight + (boardParams.cardWidth * 0.4);
		boardParams.t1x = boardParams.cardWidth * 0.7; // set initial margin
		boardParams.maxToffset = boardParams.cardHeight * 0.3;
		
		// create off-screen img tags for each card
		for(c; c<sortedDeck.length; c++)
		{
			cardImages[sortedDeck[c].id] = $("<img />")[0];
			cardImages[sortedDeck[c].id].src = app.loader.getCardURL(sortedDeck[c]);
			cardImages[sortedDeck[c].id].width = boardParams.cardWidth;
		}
		
		// set up the background
		drawBackgroundElements()
	}
	
	// destroy the canvas and shut down
	function shutdown()
	{
		app.freeCellModel.resetGame();
		update(true);	// send 'true' to stop the update cycle
		$("#gameScreenCanvas canvas").remove();
		app.showScreen("menuScreen");
	}
	
	// ------------------------------------------------------------------------
	// UTILITY
	// ------------------------------------------------------------------------
	
	// board dimension accessors, returns a vector, in pixels
	function getTableauxCoordinates(slot)
	{
		return{
			x: boardParams.t1x + (slot*(boardParams.cardWidth*1.1)),
			y: boardParams.tableauxTop
		}
	}
	
	function getCellCoordinates(slot)
	{
		return{
			x: boardParams.leftMargin + (slot*(boardParams.cardWidth * 1.1)),
			y: boardParams.cellTop
		}
	}
	
	function getFoundationCoordinates(slot)
	{
		return{
			x: boardParams.leftMargin + boardParams.cardWidth * 4.8 +  (slot*(boardParams.cardWidth * 1.1)),
			y: boardParams.cellTop
		}
	}
	
	function getCoordinates(pos, slot)
	{
		switch(pos)
		{
			case "t":
				return {
					x:getTableauxCoordinates(slot).x,
					y:getTableauxCoordinates(slot).y + getTableauxOffset(slot)
				};
				break;
			case "c":
				return getCellCoordinates(slot);
				break;
			case "f":
				return getFoundationCoordinates(slot);
				break;
		}
	}
	
	function getTableauxOffset(slot)
	{
		return (freecellModel.getTableaux(slot).length * 0.3 * boardParams.cardHeight);
	}
	
	// returns an object containing the game-model coordinates, from a mouse click
	function getClickLocation(x, y)
	{
		// first, check if we are within the upper area, or the tableaux
		
		// cells or foundations
		if(y<boardParams.cellTop + boardParams.cardHeight)
		{
			// cells
			if(x>boardParams.leftMargin && x < getFoundationCoordinates(0).x-(boardParams.cardWidth*0.5))
			{
				var cIter = 0;
				for(cIter; cIter<4; cIter++)
				{
					if(x>getCellCoordinates(cIter).x && x<getCellCoordinates(cIter+1).x-(0.1*boardParams.cardWidth))
						return{
							pos:"c",
							i:cIter
						};
				}
			}
			// foundations
			else if(x>getFoundationCoordinates(0).x && x<getFoundationCoordinates(3).x+boardParams.cardWidth)
			{
				console.log("true!");
			
				var fIter = 0;
				for(fIter; fIter<4; fIter++)
				{
					if(x>getFoundationCoordinates(fIter).x && x<getFoundationCoordinates(fIter+1).x-(0.1*boardParams.cardWidth))
						return{
							pos:"f",
							i:fIter
						};
				}
			}
		}
		//tableaux
		// todo: account for y position
		else
		{
			var tIter = 0;
			for(tIter; tIter<8; tIter++)
			{
				if(x>getTableauxCoordinates(tIter).x && x<getTableauxCoordinates(tIter+1).x-(0.1*boardParams.cardWidth))
					return{
						pos:"t",
						i:tIter
					};
			}
		}
		
		console.log("Didn't click on a valid element!");
		return null;
	}
	
	function updateMousePos(x, y)
	{
		mouseX = x;
		mouseY = y;
	}
	
	return {
		setup: setup,
		shutdown: shutdown,
		update: update,
		registerCardAnimation: registerCardAnimation,
		deal: deal,
		win: win,
		getClickLocation: getClickLocation,
		getCoordinates: getCoordinates,
		getTableauxOffset: getTableauxOffset,
		updateMousePos: updateMousePos,
		boardParams: boardParams
	}
})();