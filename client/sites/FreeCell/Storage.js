// provide functionality to store and load arbitrary Javascript objects using localstorage and JSON

var app = app || {};

app.storage = 
(function()
{
	var prefix = "";
	
	// set the prefix used by the storage system, used for storing data from different areas of the app without worrying about index collisions
	function setPrefix(p)
	{
		console.log("Setting storage prefix to '"+p+"'...");
		prefix = p;
	}
	
	// store an object in localStorage
	function set(handle, obj)
	{
		localStorage[prefix + handle] = JSON.stringify(obj);
	}
	
	// get an object from localStorage
	function get(handle)
	{
		try
		{
			return JSON.parse( localStorage[prefix + handle] );
		}
		catch(err)
		{
			console.log("Error getting data '"+handle+"' from localStorage!");
			console.log(err);
			return null;
		}
	}
	
	// remove an object from localStorage
	function remove(handle)
	{
		try
		{
			localStorage.removeItem(prefix + handle);
		}
		catch(err)
		{
			console.log("Error removing data '"+handle+"' from localStorage!");
			console.log(err);
		}
	}
	
	// public functions
	return {
		setPrefix: setPrefix,
		set: set,
		get: get,
		remove: remove
	}
})();