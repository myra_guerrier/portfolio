var app = app || { };

// Create a generator for the properties of the Splash screen
app.screen["splashScreen"] =
(
	function()
	{
		var firstRun = true,
		getLoadProgress;

		// disables the continue button
		function disableContinueButton()
		{
			$("#splashScreen").off("click");
			$("#splashScreen #playButton").hide();
		}
		
		// re-enables the continue button
		function enableContinueButton()
		{
			$("#splashScreen").click(
				function(event)
				{
					app.showScreen("menuScreen");
				}
			);
			
			$("#splashScreen #playButton").show();
		}
		
		// displays and updates the progress bar
		function showProgress()
		{
			var progress = $("#splashScreen .progress"),
				bar = $("#splashScreen .progress .bar"),
				percentDone = 100 * getLoadProgress();
				
			if(percentDone < 100)
			{
				progress.show();
				bar.width(percentDone + "%");
				setTimeout(showProgress, 30);
			}
			else
			{
				bar.width("100%");
				setTimeout(hideProgress, 300);
			}
		}
		
		// hides the progress bar and shows the continue button
		function hideProgress()
		{
			$("#splashScreen .progress").hide();
			enableContinueButton();
		}
		
		// Called whenever the screen is enabled
		function run(loadProgressFunction)
		{
			getLoadProgress = loadProgressFunction;
			disableContinueButton();
			showProgress();
		
			if(firstRun)
			{
				firstRun = false;
				init();
			}
		}
		
		// Called once, when the screen is first opened.
		function init()
		{
			$("#splashScreen").click(
				function(event) { app.showScreen( "menuScreen" ); }
			);
		}
		
		// Returns whatever parameters we want accessible publically
		return {
			run: run
		};
	}
)();