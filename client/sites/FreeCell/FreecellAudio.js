// provides functionality to play in-game audio specific to freecell

var app = app || {};

app.freeCellAudio = 
(function()
{
	var gameAudio = {};
	
	// set up the gameAudio object
	function init()
	{
		var gm = gameAudio;
		
		gm.click = "click";
		gm.bgMusic = {
				name:"background_loop",
				loop:true
			};
		gm.endMusic = "game_win";
		
		app.audio.addToAudioList(gm.click);
		app.audio.addToAudioList(gm.bgMusic.name);
		app.audio.addToAudioList(gm.endMusic);
	}
	
	// plays a specified in-game sound
	function playSound(sound)
	{
		// don't do anything if audio is disabled
		if(!app.settings.soundOn)
			return;
	
		var gm = gameAudio;
		
		switch(sound)
		{
			case "click":
				app.audio.play(gm.click);
				break;
			case "music":
				app.audio.play(gm.bgMusic);
				break;
			case "endgame":
				app.audio.play(gm.endMusic);
				break;
		}
	}
	
	return {
		init: init,
		playSound: playSound
	}
})();