var app = app || { };

// Create a generator for the properties of the About screen
app.screen["aboutScreen"] =
(
	function()
	{
		var firstRun = true;
	
		// Called whenever the screen is enabled
		function run()
		{
			if(firstRun)
			{
				firstRun = false;
				init();
			}
		}
		
		// Called once, when the screen is first opened.
		function init()
		{
			// set up a handler for the return button
			$("#aboutScreen button").click(
				function( event )
				{
					app.showScreen("menuScreen");
					return false;
				}
			);
		}
		
		// Returns whatever parameters we want accessible publically
		return {
			run: run
		};
	}
)();