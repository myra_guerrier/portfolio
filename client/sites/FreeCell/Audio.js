// contains functionality for an audio system

var app = app || {};

app.audio =
(function()
{
	var audioObjects = {},
		currentlyPlaying = [];

	// first, use Modernizr to determine which audio extensions we can use

	var formatExt;
	var extensions = ["ogg", "mp3"],
		i = 0;
		
	for(i; i<extensions.length; i++)
	{
		if(Modernizr.audio[extensions[i]] === "probably")
		{
			formatExt = extensions[i];
			console.log("We probably support "+formatExt+".");
		}
	}
	
	// then, create functionality to create and store an Audio object from a filename and add it to the global audioObjects list
	
	function addToAudioList(filename)
	{
		var audioObj = new Audio("audio/"+filename+"."+formatExt);
		
		// if we have never set up an audio object with this handle before, create the list in the first place
		if(audioObjects[filename] === undefined)
			audioObjects[filename] = [audioObj];
			
		// if there are already <Audio> tags set up, instead push the object to the list
		else
			audioObjects[filename].push(audioObj);
		
		$(audioObj).on(
			"ended",
			function(event)
			{
				currentlyPlaying.splice(currentlyPlaying.indexOf(filename), 1);
			});
		
		// return what we just generated (used by getAudio())
		return audioObjects[filename][audioObjects[filename].length - 1];
	}
	
	// returns a reference to the first Audio object of the given handle which is not playing
	function getAudio(handle)
	{
		var i = 0;
		
		for(i; i<audioObjects[handle].length; i++)
		{
			if(audioObjects[handle][i].ended)
				return audioObjects[handle][i];
			else
				return addToAudioList(handle, handle);
		}
	}
	
	// play a sound
	function play(obj)
	{
		if(typeof(obj) === "string")
		{
			currentlyPlaying.push(obj);
			console.log(currentlyPlaying);
			
			getAudio(obj).play();
			return;
		}
		
		if(typeof(obj.name) === "string")
		{
			var aObj = getAudio(obj.name);
			
			currentlyPlaying.push(obj.name);
			console.log(currentlyPlaying);
			
			aObj.loop = obj.loop;
			aObj.play();
			
			return;
		}
	}
	
	// get function for the currently playing list
	function getPlayingSounds()
	{
		return currentlyPlaying;
	}
	
	// stops all playing sounds
	function stopAll()
	{
		var i = 0,
			j = 0;
		
		for(i; i<currentlyPlaying.length; i++)
		{
			for(j; j<audioObjects[currentlyPlaying[i]].length; j++)
			audioObjects[currentlyPlaying[i]][j].pause();
		}
	}
	
	// functions which should be publically accessible
	return {
		addToAudioList: addToAudioList,
		getAudio: getAudio,
		getPlayingSounds: getPlayingSounds,
		play: play,
		stopAll: stopAll
	}

})();