// provide functionality for loading everything that this app requires

var app = app || {};

app.loader = 
(function()
{
	var fileList = [
			"src/jquery.min.js",
			"src/App.js",
			"src/Vector.js",
			"src/Time.js",
			"src/AnimatedBackground.js",
			"src/onLoad.js"
		];
		
	function run()
	{
		Modernizr.load({
			load : fileList,
			complete : function() { app.start(); }
		});
	}
	
	return {
		run: run
	}
})();

app.loader.run();