// generates and animates a background, using a canvas element

var app = app || {};

app.animatedBackground = 
(function()
{
	var firstRun = true,
		backgroundDiv,
		canvas,
		ctx,
		simulationParams = {
			objectLimit : 100,
			objectSize : 1,
			objectSizeVariance : 2,
			board : {},
			spawnRate : 0.01,
			fillColor : [
					"rgba(255,255,255,",
					"rgba(255,255,226,",
					"rgba(226,255,248,",
					"rgba(255,236,149,",
					"rgba(112,245,255,"
				],
			shadowColor : [
					"rgba(255,255,255,",
					"rgba(255,255,140,",
					"rgba(198,255,241,",
					"rgba(255,192,105,",
					"rgba(58,196,206,"
				],
			alphaMin : 0.5,
			alphaMax : 1.0,
			alphaRate : 3,
			fadeInTime : 2,
			driftRate : 15
		},
		simulationList = [],
		startTime,
		lastUpdateTime;
		
	// create the canvas and start animating it
	function init()
	{
		if(firstRun)
		{
			firstRun = false;
			
			startTime = app.time.getTime();
			
			backgroundDiv = $("#backgroundCanvas");
			canvas = $("<canvas />")[0];
			
			simulationParams.board.width = backgroundDiv.width();
			simulationParams.board.height = backgroundDiv.height();
			
			canvas.width = simulationParams.board.width;
			canvas.height = simulationParams.board.height;
			
			$(canvas).appendTo(backgroundDiv);
			
			ctx = canvas.getContext("2d");
			
			ctx.beginPath();
			ctx.rect(50,50,100,100);
			ctx.fill();
			
			// start the simulation
			update();
		}
	}
	
	// spawns a particle
	function spawnParticle()
	{
		var p = {},
			sp = simulationParams;
		
		// put the particle at a random location on the board
		p.coordinates = {
			x : Math.random() * sp.board.width,
			y : Math.random() * sp.board.height
		};
		
		p.radius = sp.objectSize + (Math.random() * sp.objectSizeVariance);
		
		p.direction = app.vector.normalize({
			x : Math.cos(2 * Math.PI * Math.random()),
			y : Math.cos(2 * Math.PI * Math.random())
		});
		
		var id = Math.floor(Math.random() * sp.fillColor.length);
		
		p.fillColor = sp.fillColor[id];
		p.shadowColor = sp.shadowColor[id];
		
		p.alpha = 0;
		
		p.fadeIn = 0;
		
		simulationList.push(p);
	}
	
	// update the simulation state
	function update()
	{
		var deltaTime = app.time.getTime() - lastUpdateTime,
			sp = simulationParams;
		lastUpdateTime = app.time.getTime();
	
		ctx.clearRect(0,0,sp.board.width,sp.board.height);
		
		// spawn particles, one by one
		if(simulationList.length < sp.objectLimit)
		{
			if(startTime + (sp.spawnRate * simulationList.length)  < app.time.getTime())
				spawnParticle();
		}
		
		// draw the particles that have been generated so far
		
		var i = 0,
			g;
			/*gradientTemplate = function(obj)
			{
				console.log(obj.radius);
			
				return ctx.createRadialGradient(
					obj.coordinates.x,
					obj.coordinates.y,
					0,
					obj.coordinates.x,
					obj.coordinates.y,
					obj.radius
				);
				
			};*/
		
		for(i; i<simulationList.length; i++)
		{
			//g = gradientTemplate(simulationList[i]);
				
			//g.addColorStop(0, "rgba(255,255,255,1)");
			//g.addColorStop(1, "rgba(255,255,255,0)");
			
			ctx.fillStyle = simulationList[i].fillColor +
				((simulationList[i].alpha * (sp.alphaMax - sp.alphaMin)) + sp.alphaMin) + ")";
				
			ctx.shadowColor = simulationList[i].shadowColor + "1)";
				
			ctx.shadowBlur = simulationList[i].radius / 2;
		
			ctx.beginPath();
			ctx.arc(simulationList[i].coordinates.x, simulationList[i].coordinates.y,
				simulationList[i].radius,
				0, 2 * Math.PI, false);
			ctx.fill();
			
			// update the position of the dust mote
			simulationList[i].coordinates = app.vector.add(
					simulationList[i].coordinates,
					app.vector.scalarMul(simulationList[i].direction, (sp.driftRate * deltaTime))
				);
				
			// update the dust mote's alpha
			/*if(simulationList[i].alpha < 1)
				simulationList[i].alpha = simulationList[i].alpha + (deltaTime / sp.alphaRate);
				
			else if(simulationList[i].alpha > 1)
				simulationList[i].alpha = simulationList[i].alpha - (deltaTime / sp.alphaRate);*/
				
			if(simulationList[i].fadeIn < 1)
				simulationList[i].fadeIn = simulationList[i].fadeIn + (deltaTime / sp.fadeInTime);
				
			simulationList[i].alpha = ((Math.cos(
					lastUpdateTime / sp.alphaRate
				) + 1) / 2) * simulationList[i].fadeIn;
				
			// set up some convenience references
			var pos = simulationList[i].coordinates,
				r = simulationList[i].radius;
				
			// check and make sure that the mote is still within the window, if it is not, wrap it
			if(pos.x < 0 - r)
				simulationList[i].coordinates.x = sp.board.width + r;
				
			if(pos.x > sp.board.width + r)
				simulationList[i].coordinates.x = 0 - r;
				
			if(pos.y < 0 - r)
				simulationList[i].coordinates.y = sp.board.height + r;
				
			if(pos.y > sp.board.height + r)
				simulationList[i].coordinates.y = 0 - r;
		}
		
		setTimeout(
			update,
			50
		);
	}
	
	return {
		init: init
	}
})();