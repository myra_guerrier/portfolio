$(function() {

	var $el, leftPos, newWidth,
		$mainNav = $("#navMenu");
	
	$mainNav.append("<li id='magic-line'></li>");
	var $magicLine = $("#magic-line");
	
	$magicLine
		.width($("a.current").parent().width())
		.css("left", $("a.current").position().left)
		.data("origLeft", $magicLine.position().left)
		.data("origWidth", $magicLine.width());
		
	$("#navMenu li a").hover(function() {
		$el = $(this);
		leftPos = $el.position().left;
		newWidth = $el.parent().width();
		$magicLine.stop().animate({
			left: leftPos,
			width: newWidth
		});
	}, function() {
		$magicLine.stop().animate({
			left: $magicLine.data("origLeft"),
			width: $magicLine.data("origWidth")
		});	
	});
});